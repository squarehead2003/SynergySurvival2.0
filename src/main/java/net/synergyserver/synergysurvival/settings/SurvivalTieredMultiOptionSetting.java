package net.synergyserver.synergysurvival.settings;

import net.synergyserver.synergycore.settings.SettingCategory;
import net.synergyserver.synergycore.settings.SettingOption;
import net.synergyserver.synergycore.settings.TieredMultiOptionSetting;
import org.bukkit.Material;

/**
 * Represents a <code>MultiOptionSetting</code> handled by SynergySurvival.
 */
public class SurvivalTieredMultiOptionSetting extends TieredMultiOptionSetting {

    public static final SurvivalTieredMultiOptionSetting DIFFICULTY = new SurvivalTieredMultiOptionSetting("difficulty",
            SettingCategory.SURVIVAL, "Changes your personal difficulty in Survival.",
            "syn.setting.difficulty", "extremely_easy", "extremely_easy",
            new SettingOption[]{
                    new SettingOption("tranquil", "syn.setting.difficulty.tranquil",
                            "Nearly vanilla but with throwable bricks and flint.", Material.LIME_DYE),
                    new SettingOption("extremely_easy", "syn.setting.difficulty.extremely_easy",
                            "Miscellaneous modifications to make Survival more \"fun\".", Material.LIME_WOOL),
                    new SettingOption("very_easy", "syn.setting.difficulty.very_easy",
                            "Natural regeneration? What natural regeneration?", Material.LIME_CONCRETE),
                    new SettingOption("really_easy", "syn.setting.difficulty.really_easy",
                            "Let's face it, you didn't want to play Survival after dying anyways.", Material.LIME_CONCRETE_POWDER),
                    new SettingOption("quite_easy", "syn.setting.difficulty.quite_easy",
                            "♩ the sun is a deadly lazer ♩", Material.LIME_STAINED_GLASS),
                    new SettingOption("pretty_easy", "syn.setting.difficulty.pretty_easy",
                            "Why are you hitting yourself? Why are you hitting yourself?", Material.LIME_STAINED_GLASS_PANE),
                    new SettingOption("somewhat_easy", "syn.setting.difficulty.somewhat_easy",
                            "Let's face it, you didn't want your inventory after dying anyways.", Material.LIME_SHULKER_BOX),
                    new SettingOption("easy", "syn.setting.difficulty.easy",
                            ":)", Material.LIME_GLAZED_TERRACOTTA)
    });

    private SurvivalTieredMultiOptionSetting(String id, SettingCategory category, String description, String permission,
                                   String defaultValue, String defaultValueNoPermission, SettingOption[] options) {
        super(id, category, description, permission, defaultValue, defaultValueNoPermission, options);
    }
}
