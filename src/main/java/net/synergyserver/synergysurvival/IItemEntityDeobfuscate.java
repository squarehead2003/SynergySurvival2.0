package net.synergyserver.synergysurvival;

import net.minecraft.world.entity.item.EntityItem;

public interface IItemEntityDeobfuscate extends IEntityDeobfuscate {

    default EntityItem get() {
        return (EntityItem) this;
    }

    default int getPickupDelay () {
        return get().ap;
    }

    default void setPickupDelay(int val) {
        get().ap = val;
    }
}
