package net.synergyserver.synergysurvival;

import com.comphenix.protocol.ProtocolLibrary;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.SynergyPlugin;
import net.synergyserver.synergycore.commands.CommandManager;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.database.MongoDB;
import net.synergyserver.synergycore.settings.SettingManager;
import net.synergyserver.synergycore.utils.TimeUtil;
import net.synergyserver.synergysurvival.commands.DeathsCheckCommand;
import net.synergyserver.synergysurvival.commands.DeathsClearCommand;
import net.synergyserver.synergysurvival.commands.DeathsCommand;
import net.synergyserver.synergysurvival.commands.DeathsTopCommand;
import net.synergyserver.synergysurvival.commands.a1723y8ou9asiodfh9hio1fiaosdfoCommand;
import net.synergyserver.synergysurvival.listeners.EntityListener;
import net.synergyserver.synergysurvival.listeners.PacketListeners;
import net.synergyserver.synergysurvival.listeners.PlayerListener;
import net.synergyserver.synergysurvival.listeners.SynergyListener;
import net.synergyserver.synergysurvival.listeners.WorldListener;
import net.synergyserver.synergysurvival.settings.SurvivalTieredMultiOptionSetting;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

/**
 * The main class for SynergySurvival.
 */
public class SynergySurvival extends JavaPlugin implements SynergyPlugin {

    @Override
    public void onEnable() {
        getLogger().info("Hooking into SynergyCore...");
        SynergyCore.getPlugin().registerSynergyPlugin(this);

        getLogger().info("Starting the sun damage timer...");
        long sunDamageTimerInterval = TimeUtil.toFlooredUnit(TimeUtil.TimeUnit.SECOND,
                TimeUtil.TimeUnit.GAME_TICK, PluginConfig.getConfig(this).getLong("sun_damage.check_interval_seconds"));
        Bukkit.getScheduler().runTaskTimer(this, SunDamageTimer.sunDamageTimer, 0L, sunDamageTimerInterval);
    }

    @Override
    public void onDisable() {
        getLogger().info("Saving plugin configuration...");
        PluginConfig.getInstance().save(this);

        getLogger().info("Saving messages...");
        Message.getInstance().save(this);
    }

    public void registerCommands() {
        CommandManager cm = CommandManager.getInstance();

        cm.registerMainCommand(this, DeathsCommand.class);
        cm.registerSubCommand(this, DeathsCheckCommand.class);
        cm.registerSubCommand(this, DeathsTopCommand.class);

        cm.registerMainCommand(this, DeathsClearCommand.class);

        cm.registerMainCommand(this, a1723y8ou9asiodfh9hio1fiaosdfoCommand.class);
    }

    public void registerSettings() {
        SettingManager sm = SettingManager.getInstance();
        sm.registerSettings(SurvivalTieredMultiOptionSetting.class);
    }

    public void registerListeners() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new SynergyListener(), this);
        pm.registerEvents(new PlayerListener(), this);
        pm.registerEvents(new EntityListener(), this);
        pm.registerEvents(new WorldListener(), this);
        // pm.registerEvents(new ItemTracker(), this);

        ProtocolLibrary.getProtocolManager().addPacketListener(PacketListeners.bossBar);
    }

    public void mapClasses() {
        MongoDB db = MongoDB.getInstance();
        db.mapClasses(
                SurvivalWorldGroupProfile.class
        );
    }

    /**
     * Convenience method for getting this plugin.
     *
     * @return The object representing this plugin.
     */
    public static SynergySurvival getPlugin() {
        return (SynergySurvival) getProvidingPlugin(SynergySurvival.class);
    }

    public String[] getAliases() {
        return new String[]{getName(), "SynSurvival", "Survival", "SynSU", "SU"};
    }

    /**
     * Gets the list of worlds that this plugin handles.
     *
     * @return The world group of this plugin.
     */
    public static List<String> getWorldGroup() {
        return PluginConfig.getConfig(getPlugin()).getStringList("world_group.worlds");
    }

    /**
     * Gets the name of the world group that this plugin handles.
     *
     * @return The world group name of this plugin.
     */
    public static String getWorldGroupName() {
        return PluginConfig.getConfig(getPlugin()).getString("world_group.name");
    }
}