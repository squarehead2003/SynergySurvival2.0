package net.synergyserver.synergysurvival.projectiles;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_19_R1.CraftServer;
import org.bukkit.craftbukkit.v1_19_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_19_R1.entity.CraftLargeFireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.CreatureSpawnEvent;

import java.util.Set;

/**
 * Represents the Bukkit wrapper for a <code>SeekingFireballBase</code>.
 */
public class SeekingFireball extends CraftLargeFireball {

    public SeekingFireball(Location location, LivingEntity shooter) {
        super((CraftServer) Bukkit.getServer(), new SeekingFireballBase(location, shooter));

        // Spawn the entity in the world
        getHandle().setBukkitEntity(this);
        ((CraftWorld) location.getWorld()).getHandle().addFreshEntity(getHandle(), CreatureSpawnEvent.SpawnReason.CUSTOM);
    }

    @Override
    public Spigot spigot() {
        return null;
    }

    @Override
    public SeekingFireballBase getHandle() {
        return (SeekingFireballBase) entity;
    }

    /**
     * Checks whether this fireball is currently seeking players.
     *
     * @return True if it's trying to target players.
     */
    public boolean isSeeking() {
        return getHandle().isSeeking();
    }

    /**
     * Sets whether this fireball is seeking players.
     *
     * @param seeking True to try to target players.
     */
    public void setSeeking(boolean seeking) {
        getHandle().setSeeking(seeking);
    }

    @Override
    public int getPortalCooldown() {
        return 0;
    }

    @Override
    public void setPortalCooldown(int i) {

    }

    @Override
    public Set<String> getScoreboardTags() {
        return null;
    }

    @Override
    public boolean addScoreboardTag(String s) {
        return false;
    }

    @Override
    public boolean removeScoreboardTag(String s) {
        return false;
    }
}
