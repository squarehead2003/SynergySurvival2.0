package net.synergyserver.synergysurvival.projectiles;

import net.minecraft.server.MinecraftServer;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EnumMoveType;
import net.minecraft.world.entity.item.EntityItem;
import net.minecraft.world.level.IBlockAccess;
import net.minecraft.world.level.RayTrace;
import net.minecraft.world.phys.MovingObjectPosition;
import net.minecraft.world.phys.Vec3D;
import net.synergyserver.synergysurvival.IItemEntityDeobfuscate;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_19_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_19_R1.inventory.CraftItemStack;
import org.bukkit.craftbukkit.v1_19_R1.entity.CraftEntity;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.lang.reflect.Field;

/**
 * Represents a custom projectile that uses an item as its representation in the world.
 */
public abstract class ItemProjectile extends EntityItem implements IItemEntityDeobfuscate {

    private Player shooter;

    /**
     * Creates a new <code>ItemProjectile</code> with the given parameters.
     *
     * @param location The location to spawn this projectile at.
     * @param shooter The player who shot this projectile.
     * @param itemStack The item to base this projectile on.
     */
    public ItemProjectile(Location location, Player shooter, ItemStack itemStack) {
        super(((CraftWorld) location.getWorld()).getHandle(), location.getX(), location.getY(), location.getZ(),
                CraftItemStack.asNMSCopy(itemStack));
        this.shooter = shooter;

        // Set the velocity of this projectile
        Vector direction = shooter.getEyeLocation().getDirection().multiply(getSpeed());
        setMot(direction.getX(), direction.getY(), direction.getZ());
        velocityChanged();

        // Set the pickup delay
        setPickupDelay(Short.MAX_VALUE);
    }

    /**
     * Gets the shooter of this <code>ItemProjectile</code>.
     *
     * @return The shooter of this <code>ItemProjectile</code>.
     */
    public Player getShooter() {
        return shooter;
    }

    /**
     * Gets the Bukkit Location object of this projectile.
     *
     * @return The Location of this projectile.
     */
    public Location getLocation() {
        return new Location(world().getWorld(), locX(), locY(), locZ(), getYaw(), getPitch());
    }

    @Override
    public void k() { //tick
        // Required by all entities
        if (!world().y) { //isClientSide
            setFlag(6, this.isGlowing());
        }
        an(); //entityBaseTick

        // Update ticks to act properly as an EntityItem
        int elapsedTicks = MinecraftServer.currentTick - getLastTick();
        int age = getAge();
        if (this.getPickupDelay() != 32767) {
            this.setPickupDelay(this.getPickupDelay() - elapsedTicks);
        }
        if (age != -32768) {
            setAge(elapsedTicks + age);
        }
        setLastTick(MinecraftServer.currentTick);

        // Kill the entity if its exceeded its life
        if (age >= getLife()) {
            die();
            return;
        }

        // Calculate the current and future position of this projectile
        Location currentLocation = new Location(world().getWorld(), locX(), locY(), locZ());
        Vec3D mot = getMot();
        double futureX = locX() + mot.c; //x
        double futureY = locY() + mot.d; //y
        double futureZ = locZ() + mot.e; //z
        Vec3D fromVector = new Vec3D(locX(), locY(), locZ());
        Vec3D toVector = new Vec3D(futureX, futureY, futureZ);

        // Check if any entities need to be damaged
        Damageable nearestEntity = null;
        double nearestEntityDistance = Double.MAX_VALUE;
        for (Entity entity : world().a_(this, this.getBoundingBox().a(mot.c, mot.d, mot.e))) { //getEntities
            CraftEntity craftEntity = entity.getBukkitEntity();

            // If the entity is the player, ignore this entity
            if (craftEntity.equals(shooter)) {
                continue;
            }

            // If the entity is not an instance of Damagable, ignore this entity
            if (!(craftEntity instanceof Damageable)) {
                continue;
            }

            double distance = currentLocation.distanceSquared(craftEntity.getLocation());

            // If the entity is further than the closest entity, ignore this entity
            if (distance > nearestEntityDistance) {
                continue;
            }

            // Update the nearest entity
            nearestEntity = (Damageable) craftEntity;
            nearestEntityDistance = distance;
        }

        // If an entity was found that should be damaged, do it and remove this projectile
        if (nearestEntity != null) {
            hitEntity(nearestEntity);
            if (dead()) {
                return;
            }
        }

        // Find if this projectile is going to hit a block
        MovingObjectPosition movingObjectPosition = this.world().getWorld().getHandle().a(new RayTrace(fromVector, toVector,
                RayTrace.BlockCollisionOption.a /*Collider*/, RayTrace.FluidCollisionOption.a /*NONE*/, this));

        // If it's going to hit a block then remove this projectile
        if (movingObjectPosition != null) {
            Vec3D blockPosition = movingObjectPosition.e(); //getPos
            Location blockLocation = new Location(world().getWorld(), blockPosition.a(), blockPosition.b(), blockPosition.c());
            hitBlock(blockLocation.getBlock());
            if (dead()) {
                return;
            }
        }

        double gravity = .98;

        if(isInWater()) {
            // Create bubbles
            world().getWorld().spawnParticle(Particle.WATER_BUBBLE, getLocation(), 4);

            // Make the projectile slow down in water
            gravity = 0.9D;
        }

        // Update the inertia
        Vec3D newMot = mot.a(gravity);
        setMot(newMot);

        // Move the projectile
        move(EnumMoveType.a, newMot); //EnumMoveType.SELF
    }

    public int getLastTick() {
        // Make lastTick accessible and get it
        try {
            Field f = EntityItem.class.getDeclaredField("lastTick");
            f.setAccessible(true);
            return f.getInt(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return MinecraftServer.currentTick;
    }

    public void setLastTick(int lastTick) {
        // Make lastTick accessible and set it
        try {
            Field f = EntityItem.class.getDeclaredField("lastTick");
            f.setAccessible(true);
            f.setInt(this, lastTick);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getAge() {
        // Make age accessible and get it
        try {
            Field f = EntityItem.class.getDeclaredField("age");
            f.setAccessible(true);
            return f.getInt(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void setAge(int age) {
        // Make age accessible and set it
        try {
            Field f = EntityItem.class.getDeclaredField("age");
            f.setAccessible(true);
            f.setInt(this, age);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets how many ticks this projectile should exist for before being destroyed.
     *
     * @return The life of this projectile, in ticks.
     */
    public int getLife() {
        return 100;
    }

    /**
     * Gets the speed of this projectile.
     *
     * @return The speed of this projectile.
     */
    public double getSpeed() {
        return 1;
    }

    /**
     * Affects the given entity.
     *
     * @param entity The entity to affect.
     */
    public void hitEntity(Damageable entity) {
        die();
    }

    /**
     * Affects the given block.
     *
     * @param block The block to affect.
     */
    public void hitBlock(Block block) {
        die();
    }

    /**
     * Sets the Bukkit entity of this projectile.
     *
     * @param entity The Bukkit entity of this projectile.
     */
    public void setBukkitEntity(CraftEntity entity) {
        try {
            Field f = Entity.class.getDeclaredField("bukkitEntity");
            if (!f.isAccessible()) {
                f.setAccessible(true);
            }
            f.set(this, entity);
        } catch (NoSuchFieldException|IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
