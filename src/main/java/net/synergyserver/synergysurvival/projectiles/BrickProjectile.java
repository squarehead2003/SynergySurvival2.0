package net.synergyserver.synergysurvival.projectiles;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_19_R1.CraftServer;
import org.bukkit.craftbukkit.v1_19_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_19_R1.entity.CraftItem;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.CreatureSpawnEvent;

import java.util.Set;

/**
 * Represents the Bukkit wrapper for a <code>BrickProjectileBase</code>.
 */
public class BrickProjectile extends CraftItem {

    public BrickProjectile(Location location, Player shooter) {
        super((CraftServer) Bukkit.getServer(), new BrickProjectileBase(location, shooter));

        // Spawn the entity in the world
        getHandle().setBukkitEntity(this);
        ((CraftWorld) location.getWorld()).getHandle().addFreshEntity(getHandle(), CreatureSpawnEvent.SpawnReason.CUSTOM);
    }

    @Override
    public Spigot spigot() {
        return null;
    }

    @Override
    public BrickProjectileBase getHandle() {
        return (BrickProjectileBase) entity;
    }

    @Override
    public int getPortalCooldown() {
        return 0;
    }

    @Override
    public void setPortalCooldown(int i) {

    }

    @Override
    public Set<String> getScoreboardTags() {
        return null;
    }

    @Override
    public boolean addScoreboardTag(String s) {
        return false;
    }

    @Override
    public boolean removeScoreboardTag(String s) {
        return false;
    }
}
