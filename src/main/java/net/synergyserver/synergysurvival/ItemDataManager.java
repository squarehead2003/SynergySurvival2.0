package net.synergyserver.synergysurvival;

import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.database.MongoDB;
import net.synergyserver.synergycore.utils.ItemUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.mongodb.morphia.Datastore;

import java.util.HashMap;
import java.util.List;

/**
 * Represents a <code>ItemDataManager</code>, which handles <code>ItemData</code> objects.
 */
public class ItemDataManager {

    private static ItemDataManager instance;
    private HashMap<String, ItemData> itemData;

    /**
     * Creates a new <code>ItemDataManager</code> object.
     */
    private ItemDataManager() {
        this.itemData = new HashMap<>();
    }

    /**
     * Returns the object representing this <code>ItemDataManager</code>.
     *
     * @return The object of this class.
     */
    public static ItemDataManager getInstance() {
        if (instance == null) {
            instance = new ItemDataManager();
        }
        return instance;
    }

    /**
     * Loads all <code>ItemData</code> from the database into the HashMap.
     */
    public void load() {
        Datastore ds = MongoDB.getInstance().getDatastore();
        List<ItemData> itemDataList = ds.find(ItemData.class).asList();

        for (ItemData data : itemDataList) {
            itemData.put(data.getKey(), data);
        }
    }

    /**
     * Returns the HashMap containing all <code>ItemData</code>.
     *
     * @return All <code>ItemData</code>.
     */
    public HashMap<String, ItemData> getItemData() {
        return itemData;
    }

    public void add(ItemStack item) {
        if (item == null) {
            return;
        }

        add(item.getType(), item.getAmount());
    }

    public void add(ItemStack item, int amount) {
        if (item == null) {
            return;
        }

        add(item.getType(), amount);
    }

    /**
     * Adds the given amount of the given type of item to circulation.
     *
     * @param material The material of the item to add.
     * @param amount The amount to add.
     */
    public void add(Material material, int amount) {
        // Ignore the item if it's air
        if (ItemUtil.isAir(material)) {
            return;
        }

        String key = getKey(material);

        // If the targeted item isn't in the database, create a new ItemData
        if (!itemData.containsKey(key)) {
            ItemData data = new ItemData(material.name());
            DataManager.getInstance().saveDataEntity(data);
            itemData.put(key, data);
        }

        // debug
        Bukkit.broadcastMessage("Changed " + key + " by " + amount);

        // Add the given amount to the item's data
        itemData.get(key).changeCirculating(amount);
    }

    public void subtract(ItemStack item) {
        if (item == null) {
            return;
        }

        subtract(item.getType(), item.getAmount());
    }

    public void subtract(ItemStack item, int amount) {
        if (item == null) {
            return;
        }

        subtract(item.getType(), amount);
    }

    /**
     * Subtracts the given amount of the given type of item from circulation.
     *
     * @param material The material of the item to subtract.
     * @param amount The amount to subtract.
     */
    public void subtract(Material material, int amount) {
        add(material, -1 * amount);
    }

    public void ignoreAddition(ItemStack item) {
        if (item == null) {
            return;
        }

        ignoreAddition(item.getType(), item.getAmount());
    }

    public void ignoreAddition(ItemStack item, int amount) {
        if (item == null) {
            return;
        }

        ignoreAddition(item.getType(), amount);
    }

    /**
     * Alias for <code>subtract</code>. Used to counteract an expected addition
     * to the circulation by an equal subtraction, effectively ignoring the event.
     *
     * @param material The material of the item to ignore.
     * @param amount The amount to ignore.
     */
    public void ignoreAddition(Material material, int amount) {
        subtract(material, amount);
    }

    public String getKey(Material material) {
        return material.name();
    }
}
