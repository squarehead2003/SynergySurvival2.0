package net.synergyserver.synergysurvival;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EnumMoveType;
import net.minecraft.world.level.World;
import net.minecraft.world.phys.AxisAlignedBB;
import net.minecraft.world.phys.Vec3D;

public interface IEntityDeobfuscate {

    default Entity get() {
        return (Entity) this;
    }

    default double locX() {
        return get().df();
    }

    default double locY() {
        return get().dh();
    }

    default double locZ() {
        return get().dl();
    }

    default float getPitch() {
        return get().ds();
    }

    default float getYaw() {
        return get().dq();
    }

    default World world() {
        return get().s;
    }

    default boolean dead() {
        return get().dt();
    }

    default void die() {
        //TODO double check this
        get().b(Entity.RemovalReason.e); //no clue
    }

    default void setMot(Vec3D vec3D) {
        get().f(vec3D);
    }

    default void setMot(double x, double y, double z) {
        setMot(new Vec3D(x,y,z));
    }

    default Vec3D getMot() {
        return get().dd();
    }

    default boolean onGround() {
        return get().y;
    }

    default void move(EnumMoveType enummovetype, Vec3D vec3d) {
        get().a(enummovetype, vec3d);
    }

    default boolean isInWater() {
        return get().aR();
    }

    default void velocityChanged() {
        get().D = true;
    }

    default void setFlag(int i, boolean flag) {
        get().b(i, flag);
    }

    default boolean isGlowing() {
        return get().bW();
    }

    default AxisAlignedBB getBoundingBox() {
        return get().cy();
    }
}
