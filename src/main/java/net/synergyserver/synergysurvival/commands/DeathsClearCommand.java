package net.synergyserver.synergysurvival.commands;

import net.synergyserver.synergycore.SerializableLocation;
import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.MainCommand;
import net.synergyserver.synergycore.database.MongoDB;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergysurvival.SurvivalWorldGroupProfile;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;

import java.util.List;

@CommandDeclaration(
        commandName = "a230rufasfu9asihodfpo1jhoiallf",
        permission = "syn.a230rufasfu9asihodfpo1jhoiallf",
        usage = "/a230rufasfu9asihodfpo1jhoiallf <world>",
        description = "Data migration command.",
        minArgs = 1,
        maxArgs = 1
)
public class DeathsClearCommand extends MainCommand {

    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        MongoDB db = MongoDB.getInstance();
        List<WorldGroupProfile> wgps = db.getDatastore().find(WorldGroupProfile.class).field("n").equal("Survival")
                .retrievedFields(true, "_id", "pid").asList();

        for (WorldGroupProfile wgp : wgps) {
            SurvivalWorldGroupProfile swgp = (SurvivalWorldGroupProfile) wgp;
            swgp.setDeaths(0);
            swgp.setLastDeath(0);
            swgp.setLastLocation(new SerializableLocation(new Location(Bukkit.getWorld(args[0]), 0, 0, 0)));

            Bukkit.broadcastMessage("Reset SWGP of " + PlayerUtil.getName(swgp.getPlayerID()));
        }

        return true;
    }
}
