package net.synergyserver.synergysurvival.commands;

import net.minecraft.world.entity.boss.enderdragon.phases.DragonControllerPhase;
import net.minecraft.world.level.levelgen.feature.WorldGenEnder;
import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.MainCommand;
import net.synergyserver.synergycore.commands.SenderType;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergysurvival.SynergySurvival;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_19_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_19_R1.entity.CraftEnderDragon;
import org.bukkit.entity.EnderDragon;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wither;
import org.bukkit.metadata.FixedMetadataValue;

@CommandDeclaration(
        commandName = "a1723y8ou9asiodfh9hio1fiaosdfoC",
        permission = "syn.a1723y8ou9asiodfh9hio1fiaosdfo",
        usage = "/a1723y8ou9asiodfh9hio1fiaosdfo",
        maxArgs = 0,
        validSenders = SenderType.PLAYER
)
public class a1723y8ou9asiodfh9hio1fiaosdfoCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        if (PluginConfig.getConfig(SynergySurvival.getPlugin()).getBoolean("enable_dragon")) {
            sender.sendMessage("Dragon fight is already enabled");
        }

        PluginConfig.getConfig(SynergySurvival.getPlugin()).set("enable_dragon", true);
        PluginConfig.getInstance().save(SynergySurvival.getPlugin());

        Player player = (Player) sender;
        World world = player.getWorld();

        if (world.getEnvironment() != World.Environment.THE_END) {
            player.sendMessage("You must be in the end to use this command");
        }

        // Replace ender crystals just in case
        for (WorldGenEnder.Spike spike : WorldGenEnder.a(((CraftWorld) world).getHandle())) {
            int y = world.getHighestBlockYAt(spike.a(), spike.b());
            Location center = new Location(world, spike.a() + 0.5D, y, spike.b() + 0.5D);
            world.spawnEntity(center, EntityType.ENDER_CRYSTAL);
        }

        for (int i = 0; i < 3; i++) {
            Wither wither = world.spawn(new Location(world, MathUtil.randInt(-25, 25), 64, MathUtil.randInt(-25, 25)), Wither.class);
            wither.setMetadata("synOnlyTargetPlayers", new FixedMetadataValue(SynergySurvival.getPlugin(), true));
        }
        for (int i = 0; i < 3; i++) {
            EnderDragon dragon = world.spawn(new Location(world, MathUtil.randInt(-50, 50), 100, MathUtil.randInt(-50, 50)), EnderDragon.class);
            ((CraftEnderDragon) dragon).getHandle().fG()/*getDragonControllerManager*/.a/*setControllerPhase*/(DragonControllerPhase.a);
            ((CraftEnderDragon) dragon).getHandle().b(0.0D, 128.0D, 0.0D, MathUtil.randInt(0, 360), 0.0F); //setPositionRotation
        }

        for (Player p : Bukkit.getOnlinePlayers()) {
            if (SynergySurvival.getWorldGroup().contains(p.getWorld().getName())) {
                p.sendMessage(Message.get("dragon_fight_start"));
            }
        }

        return true;
    }
}
