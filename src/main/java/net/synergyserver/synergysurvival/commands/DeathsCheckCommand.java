package net.synergyserver.synergysurvival.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.SenderType;
import net.synergyserver.synergycore.commands.SubCommand;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergysurvival.SurvivalWorldGroupProfile;
import net.synergyserver.synergysurvival.SynergySurvival;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "check",
        aliases = {"c", "get", "progress", "show"},
        permission = "syn.deaths.check",
        usage = "/deaths check [player]",
        description = "Checks the number of deaths of yourself or another player in the Survival world group.",
        maxArgs = 1,
        parentCommandName = "deaths"
)
public class DeathsCheckCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        boolean isSelf = false;
        UUID pID;
        MinecraftProfile mcp;

        if (args.length == 0) {
            // If args.length is 0 then that means that the sender is required to be a player checking their own deaths
            if (!SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
                sender.sendMessage(Message.get("commands.error.sender_type_requires_player"));
                return false;
            }

            isSelf = true;
            // Get the UUID of the sender
            pID = ((Player) sender).getUniqueId();
        } else {
            // Get the referenced player
            pID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));

            // If no player was found then give the sender an error message
            if (pID == null) {
                sender.sendMessage(Message.format("commands.error.player_not_found", args[0]));
                return false;
            }
        }

        String pName = PlayerUtil.getName(pID);

        if (PlayerUtil.isOnline(pID)) {
            mcp = PlayerUtil.getProfile(pID);
        } else {
            mcp = DataManager.getInstance().getPartialDataEntity(MinecraftProfile.class, pID, "wp");
        }

        // Check if the player has data for the Survival worldgroup
        if (!mcp.hasWorldGroupProfile(SynergySurvival.getWorldGroupName())) {
            // If the player specified is the sender, then give them a special message
            if (isSelf) {
                sender.sendMessage(Message.format("commands.deaths.check.info.world_group_no_data_self"));
                return true;
            }

            sender.sendMessage(Message.format("commands.deaths.check.info.world_group_no_data_other", pName));
            return true;
        }

        SurvivalWorldGroupProfile wgp = mcp.getPartialWorldGroupProfile(SurvivalWorldGroupProfile.class, SynergySurvival.getWorldGroupName(), "d");

        // If the player specified is the sender, then give them special feedback
        if (isSelf) {
            sender.sendMessage(Message.format("commands.deaths.check.info.self", wgp.getDeaths()));
            return true;
        } else {
            sender.sendMessage(Message.format("commands.deaths.check.info.other", pName, wgp.getDeaths()));
            return true;
        }
    }
}
