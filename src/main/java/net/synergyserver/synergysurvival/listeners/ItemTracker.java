package net.synergyserver.synergysurvival.listeners;

import net.synergyserver.synergysurvival.ItemDataManager;
import net.synergyserver.synergysurvival.SynergySurvival;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Boat;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Hanging;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Vehicle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.inventory.BrewEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.FurnaceBurnEvent;
import org.bukkit.event.inventory.FurnaceSmeltEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerEditBookEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemBreakEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.vehicle.VehicleCreateEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.inventory.BeaconInventory;
import org.bukkit.inventory.BrewerInventory;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.util.Vector;

import java.util.HashMap;

/**
 * Listens to all events that affect the number of items in circulation.
 */
public class ItemTracker implements Listener {

    private ItemDataManager idm = ItemDataManager.getInstance();

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        Item item = event.getItem();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(item.getWorld().getName())) {
            return;
        }

        // Ignore the event if it's not an arrow
        if (!(item instanceof Arrow)) {
            return;
        }

        // Add picked up arrows to the circulation if they were shot by players
        Arrow arrow = (Arrow) item;
        if (arrow.getShooter() != null && arrow.getShooter() instanceof Player) {
            idm.add(item.getItemStack());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        Item item = event.getItemDrop();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(item.getWorld().getName())) {
            return;
        }

        // Ignore items that are dropped by players
        idm.ignoreAddition(item.getItemStack());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        Block block = event.getBlock();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(block.getWorld().getName())) {
            return;
        }

        // Ignore the event if it's not an inventory holder
        if (!(block instanceof InventoryHolder)) {
            return;
        }

        InventoryHolder inventoryHolder = (InventoryHolder) block;

        // Ignore all items that are in its inventory
        for (ItemStack itemStack : inventoryHolder.getInventory()) {
            idm.ignoreAddition(itemStack);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntityDamage(EntityDamageEvent event) {
        Entity entity = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(entity.getWorld().getName())) {
            return;
        }

        // If the entity isn't an armor stand, ignore the event
        if (!(entity instanceof ArmorStand)) {
            return;
        }

        ArmorStand armorStand = (ArmorStand) entity;

        // Delay a health check by a tick because there's no death event for armor stands
        Bukkit.getScheduler().runTaskLater(SynergySurvival.getPlugin(), new Runnable() {
            @Override
            public void run() {
                // Ignore the event if the armor stand didn't die
                if (!armorStand.isDead()) {
                    return;
                }

                // Ignore any items in the armor stand's equipment
                if (!armorStand.getHelmet().getType().equals(Material.AIR)) {
                    idm.ignoreAddition(armorStand.getHelmet());
                }
                if (!armorStand.getChestplate().getType().equals(Material.AIR)) {
                    idm.ignoreAddition(armorStand.getChestplate());
                }
                if (!armorStand.getLeggings().getType().equals(Material.AIR)) {
                    idm.ignoreAddition(armorStand.getLeggings());
                }
                if (!armorStand.getBoots().getType().equals(Material.AIR)) {
                    idm.ignoreAddition(armorStand.getBoots());
                }
                if (!armorStand.getItemInHand().getType().equals(Material.AIR)) {
                    idm.ignoreAddition(armorStand.getItemInHand());
                }
            }
        }, 1);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntityDeath(EntityDeathEvent event) {
        LivingEntity entity = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(entity.getWorld().getName())) {
            return;
        }

        // If ihe entity is an inventory holder then handle its entire inventory
        if (entity instanceof InventoryHolder) {
            InventoryHolder inventoryHolder = (InventoryHolder) entity;
            for (ItemStack itemStack : inventoryHolder.getInventory()) {
                idm.ignoreAddition(itemStack);
            }
            return;
        }

        // Otherwise, ignore any items that the mob picked up
        EntityEquipment equipment = entity.getEquipment();
        if (equipment.getHelmetDropChance() > 2) {
            idm.ignoreAddition(equipment.getHelmet());
        }
        if (equipment.getChestplateDropChance() > 2) {
            idm.ignoreAddition(equipment.getChestplate());
        }
        if (equipment.getLeggingsDropChance() > 2) {
            idm.ignoreAddition(equipment.getLeggings());
        }
        if (equipment.getBootsDropChance() > 2) {
            idm.ignoreAddition(equipment.getBoots());
        }
        if (equipment.getItemInMainHandDropChance() > 2) {
            idm.ignoreAddition(equipment.getItemInMainHand());
        }
        if (equipment.getItemInOffHandDropChance() > 2) {
            idm.ignoreAddition(equipment.getItemInOffHand());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onVehicleDestroy(VehicleDestroyEvent event) {
        Vehicle vehicle = event.getVehicle();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(vehicle.getWorld().getName())) {
            return;
        }

        // Ignore the event if the vehicle isn't an InventoryHolder
        if (!(vehicle instanceof InventoryHolder)) {
            return;
        }

        InventoryHolder inventoryHolder = (InventoryHolder) vehicle;

        // Ignore all items in its inventory
        for (ItemStack itemStack : inventoryHolder.getInventory()) {
            idm.ignoreAddition(itemStack);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onItemSpawn(ItemSpawnEvent event) {
        Item item = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(item.getWorld().getName())) {
            return;
        }

        // Add the item to circulation
        idm.add(item.getItemStack());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockDispense(BlockDispenseEvent event) {
        Block block = event.getBlock();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(block.getWorld().getName())) {
            return;
        }

        ItemStack itemStack = event.getItem();
        Bukkit.broadcastMessage("Dispensed " + itemStack.getType().name());

        // When an item is dispensed as an item, it's velocity is -1 < x < 1 but ≠ 0
        Vector vel = event.getVelocity();
        if (Math.abs(vel.getX()) < 1 && Math.abs(vel.getY()) < 1 && Math.abs(vel.getZ()) < 1 && !vel.equals(new Vector(0, 0, 0))) {
            // Subtract the dropped item from circulation to cancel its addition out
            idm.subtract(itemStack, 1);
        }

        InventoryHolder inventoryHolder = (InventoryHolder) event.getBlock();
        switch (itemStack.getType()) {
            // Subtract the current type of bucket from circulation and add what it's going to become
            case BUCKET:
                idm.subtract(Material.BUCKET, 1);

                // Count the number of the type of item that was dispensed
                int oldWaterBucketCount = 0;
                int oldLavaBucketCount = 0;
                for (ItemStack itemStack1 : inventoryHolder.getInventory()) {
                    // If it's a water or lava bucket then add it to the right total
                    if (itemStack1.getType().equals(Material.WATER_BUCKET)) {
                        oldWaterBucketCount += itemStack1.getAmount();
                    } else if (itemStack1.getType().equals(Material.LAVA_BUCKET)) {
                        oldLavaBucketCount += itemStack1.getAmount();
                    }
                }

                int finalOldWaterBucketCount = oldWaterBucketCount;
                int finalOldLavaBucketCount = oldLavaBucketCount;

                Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(SynergySurvival.getPlugin(), new Runnable() {
                    @Override
                    public void run() {
                        // Count the new number of items of the dispensed type
                        InventoryHolder inventoryHolder = (InventoryHolder) event.getBlock();
                        int newWaterBucketCount = 0;
                        int newLavaBucketCount = 0;
                        for (ItemStack itemStack1 : inventoryHolder.getInventory()) {
                            // If it's a water or lava bucket then add it to the right total
                            if (itemStack1.getType().equals(Material.WATER_BUCKET)) {
                                newWaterBucketCount += itemStack1.getAmount();
                            } else if (itemStack1.getType().equals(Material.LAVA_BUCKET)) {
                                newLavaBucketCount += itemStack1.getAmount();
                            }
                        }

                        // If the item was added then add it to circulation
                        if (newWaterBucketCount > finalOldWaterBucketCount) {
                            idm.add(Material.WATER_BUCKET, 1);
                        } else if (newLavaBucketCount > finalOldLavaBucketCount) {
                            idm.add(Material.LAVA_BUCKET, 1);
                        }
                    }
                }, 0L);
                break;
            case LAVA_BUCKET:
                idm.subtract(Material.LAVA_BUCKET, 1);
                idm.add(Material.BUCKET, 1);
                break;
            case WATER_BUCKET:
                idm.subtract(Material.WATER_BUCKET,  1);
                idm.add(Material.BUCKET, 1);
                break;
            // Only subtract bonemeal and flint and steel if they're used
            case INK_SAC:
                // Ignore all dyes except for bone meal
                if (itemStack.getDurability() != 15) {
                    break;
                }
            // Subtract flint and steel from circulation when it breaks
            case FLINT_AND_STEEL:
                // Count the number of the type of item that was dispensed
                int oldCount = 0;
                for (ItemStack itemStack1 : inventoryHolder.getInventory()) {
                    // If the type is the same then add the amount to the total
                    if (itemStack1.getType().equals(itemStack.getType())) {
                        oldCount += itemStack1.getAmount();
                    }
                }

                int finalOldCount = oldCount;

                Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(SynergySurvival.getPlugin(), new Runnable() {
                    @Override
                    public void run() {
                        // Count the new number of items of the dispensed type
                        InventoryHolder inventoryHolder = (InventoryHolder) event.getBlock();
                        int newCount = 0;
                        for (ItemStack itemStack1 : inventoryHolder.getInventory()) {
                            // If the type is the same then add the amount to the total
                            if (itemStack1.getType().equals(itemStack.getType())) {
                                newCount += itemStack1.getAmount();
                            }
                        }

                        // If the item was used then subtract it from circulation
                        if (newCount < finalOldCount) {
                            idm.subtract(itemStack, finalOldCount - newCount);
                        }
                    }
                }, 0L);
                break;
            // Ignore skulls and pumpkins unless they are used to make a mob
            case WITHER_SKELETON_SKULL:
            case PUMPKIN:
                // TODO
            default:
                break;
        }

    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onFurnaceSmelt(FurnaceSmeltEvent event) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(event.getBlock().getWorld().getName())) {
            return;
        }

        // Subtract the source item from circulation and add the product
        idm.subtract(event.getSource(), 1);
        idm.add(event.getResult(), 1);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onFurnaceBurn(FurnaceBurnEvent event) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(event.getBlock().getWorld().getName())) {
            return;
        }

        // Subtract 1 fuel item from circulation
        idm.subtract(event.getFuel(), 1);

        // If it's a lava bucket then add the bucket back to circulation
        if (event.getFuel().getType().equals(Material.LAVA_BUCKET)) {
            idm.add(Material.BUCKET, 1);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBookSign(PlayerEditBookEvent event) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(event.getPlayer().getWorld().getName())) {
            return;
        }

        // Ignore the event if they're not signing the book
        if (!event.isSigning()) {
            return;
        }

        // Subtract a book and quill from circulation and add a written book
        idm.subtract(Material.WRITABLE_BOOK, 1);
        idm.add(Material.WRITTEN_BOOK, 1);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBucketFill(PlayerBucketFillEvent event) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(event.getPlayer().getWorld().getName())) {
            return;
        }

        // Subtract the empty bucket from circulation and add the newly filled bucket
        idm.subtract(event.getBucket(), 1);
        idm.add(event.getItemStack(), 1);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBucketEmpty(PlayerBucketEmptyEvent event) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(event.getPlayer().getWorld().getName())) {
            return;
        }

        // Subtract the filled bucket from circulation and add the empty bucket
        idm.subtract(event.getBucket(), 1);
        idm.add(event.getItemStack(), 1);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onItemConsume(PlayerItemConsumeEvent event) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(event.getPlayer().getWorld().getName())) {
            return;
        }

        // Subtract the consumed item from circulation
        ItemStack itemStack = event.getItem();
        idm.subtract(itemStack, 1);

        switch (itemStack.getType()) {
            // If it's a potion then add a bottle to circulation
            case POTION:
                idm.add(Material.GLASS_BOTTLE, 1);
                break;
            // If it's a soup then add a bowl to circulation
            case MUSHROOM_STEW:
            case RABBIT_STEW:
            case BEETROOT_SOUP:
                idm.add(Material.BOWL, 1);
                break;
            // If it's milk then add a bucket to circulation
            case MILK_BUCKET:
                idm.add(Material.BUCKET, 1);
                break;
            default:
                break;
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPotionBrew(BrewEvent event) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(event.getBlock().getWorld().getName())) {
            return;
        }

        BrewerInventory inventory = event.getContents();

        // Subtract the item used to brew and add the products made
        idm.subtract(inventory.getIngredient(), 1);
        /*for (int i = 0; i < 3; i++) {
            idm.add(inventory.getItem(0));
        }*/
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onItemMake(InventoryClickEvent event) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(event.getWhoClicked().getWorld().getName())) {
            return;
        }

        // Ignore the event if they didn't click the product slot
        if (!event.getSlotType().equals(InventoryType.SlotType.RESULT)) {
            return;
        }

        Inventory inventory = event.getClickedInventory();
        ItemStack result = event.getCurrentItem();

        // Ignore the event if there was no product made
        if (result == null) {
            return;
        }

        // Anvil and trading
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onItemCraft(CraftItemEvent event) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(event.getWhoClicked().getWorld().getName())) {
            return;
        }

        // Count the number of times the recipe was made
        Recipe recipe = event.getRecipe();
        ItemStack result = event.getRecipe().getResult();

        // Count the number of items in the inventory before the craft
        HashMap<Material, Integer> oldInventoryCounts = new HashMap<>();
        ItemStack[] contents = event.getInventory().getContents();
        for (int i = 1; i < contents.length; i++) {
            ItemStack itemStack = contents[i];

            // If it's already counted then add to the total, else put a new value in
            if (oldInventoryCounts.containsKey(itemStack.getType())) {
                oldInventoryCounts.put(itemStack.getType(), oldInventoryCounts.get(itemStack.getType()) + itemStack.getAmount());
            } else {
                oldInventoryCounts.put(itemStack.getType(), itemStack.getAmount());
            }
        }

        // Count the number of result items in the player's inventory before the craft
        int oldResultCount = 0;
        for (ItemStack itemStack : event.getWhoClicked().getInventory()) {
            // Ignore the item if it's null
            if (itemStack == null) {
                continue;
            }

            // If the same type as the result item then add it to the count
            if (itemStack.getType().equals(result.getType())) {
                oldResultCount += itemStack.getAmount();
            }
        }

        // Subtract the number of items used and add the number of items crafted
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(SynergySurvival.getPlugin(), new Runnable() {
            @Override
            public void run() {
                // Count the new number of items in the inventory
                HashMap<Material, Integer> newInventoryCounts = new HashMap<>();
                ItemStack[] contents = event.getInventory().getContents();
                for (int i = 1; i < contents.length; i++) {
                    ItemStack itemStack = contents[i];

                    // If it's already counted then add to the total, else put a new value in
                    if (newInventoryCounts.containsKey(itemStack.getType())) {
                        newInventoryCounts.put(itemStack.getType(), newInventoryCounts.get(itemStack.getType()) + itemStack.getAmount());
                    } else {
                        newInventoryCounts.put(itemStack.getType(), itemStack.getAmount());
                    }
                }

                // Count the number of result items in the player's inventory after the craft
                int newResultCount = 0;
                for (ItemStack itemStack : event.getWhoClicked().getInventory()) {
                    // Ignore the item if it's null
                    if (itemStack == null) {
                        continue;
                    }

                    // If the same type as the result item then add it to the count
                    if (itemStack.getType().equals(result.getType())) {
                        newResultCount += itemStack.getAmount();
                    }
                }

                // Subtract the items used in the craft
                for (Material material : oldInventoryCounts.keySet()) {
                    // If there's no key in the new hashmap then subtract it entirely
                    if (!newInventoryCounts.containsKey(material)) {
                    }
                }
            }
        }, 0L);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onProjectileLaunch(ProjectileLaunchEvent event) {
        Projectile projectile = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(projectile.getWorld().getName())) {
            return;
        }

        // Ignore the event if the shooter wasn't a player
        if (projectile.getShooter() == null || !(projectile.getShooter() instanceof Player)) {
            return;
        }

        // Comment out since not used and to avoid version conversion
//        switch (projectile.getType()) {
//            // Subtract projectiles from circulation
//            case ARROW:
//                idm.subtract(Material.ARROW, 1);
//                break;
//            case SPECTRAL_ARROW:
//                idm.subtract(Material.SPECTRAL_ARROW, 1);
//                break;
//            case EGG:
//                idm.subtract(Material.EGG, 1);
//                break;
//            case SNOWBALL:
//                idm.subtract(Material.SNOWBALL, 1);
//                break;
//            case ENDER_PEARL:
//                idm.subtract(Material.ENDER_PEARL, 1);
//                break;
//            case THROWN_EXP_BOTTLE:
//                idm.subtract(Material.EXPERIENCE_BOTTLE, 1);
//                break;
//            // Keep track of potion data
//            case ARROW:
//                TippedArrow tippedArrow = () projectile;
//
//                ItemStack tippedArrowItem = new ItemStack(Material.TIPPED_ARROW);
//                PotionMeta tippedArrowMeta = (PotionMeta) tippedArrowItem.getItemMeta();
//                tippedArrowMeta.setBasePotionData(tippedArrow.getBasePotionData());
//                tippedArrowItem.setItemMeta(tippedArrowMeta);
//
//                idm.subtract(tippedArrowItem, 1);
//                break;
//            case SPLASH_POTION:
//            case LINGERING_POTION:
//                ThrownPotion thrownPotion = (ThrownPotion) projectile;
//                ItemStack thrownPotionItem = thrownPotion.getItem();
//                idm.subtract(thrownPotionItem, 1);
//                break;
//            default:
//                break;
//        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent event) {
        Block block = event.getBlockPlaced();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(block.getWorld().getName())) {
            return;
        }

        // Count causes of fire differently
        if (block.getType().equals(Material.FIRE)) {
            return;
        }

        // Subtract the block from circulation
        idm.subtract(block.getType(),  1);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onItemBreak(PlayerItemBreakEvent event) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(event.getPlayer().getWorld().getName())) {
            return;
        }

        // Subtract the item from circulation
        idm.subtract(event.getBrokenItem(), 1);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onItemDespawn(ItemDespawnEvent event) {
        Item item = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(item.getWorld().getName())) {
            return;
        }

        // Subtract the item from circulation
        idm.subtract(item.getItemStack());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onVehicleCreate(VehicleCreateEvent event) {
        Vehicle vehicle = event.getVehicle();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(vehicle.getWorld().getName())) {
            return;
        }

        // Subtract the vehicle from circulation
        switch (vehicle.getType()) {
            case MINECART:
                idm.subtract(Material.MINECART, 1);
                break;
            case MINECART_CHEST:
                idm.subtract(Material.CHEST_MINECART,  1);
                break;
            case MINECART_FURNACE:
                idm.subtract(Material.FURNACE_MINECART, 1);
                break;
            case MINECART_HOPPER:
                idm.subtract(Material.HOPPER_MINECART, 1);
                break;
            case MINECART_TNT:
                idm.subtract(Material.TNT_MINECART, 1);
                break;
            // Handle each boat type
            case BOAT:
                Boat boat = (Boat) vehicle;

                switch (boat.getWoodType()) {
                    case GENERIC:
                        idm.subtract(Material.OAK_BOAT, 1);
                        break;
                    case REDWOOD:
                        idm.subtract(Material.SPRUCE_BOAT, 1);
                        break;
                    case BIRCH:
                        idm.subtract(Material.BIRCH_BOAT, 1);
                        break;
                    case JUNGLE:
                        idm.subtract(Material.JUNGLE_BOAT, 1);
                        break;
                    case ACACIA:
                        idm.subtract(Material.ACACIA_BOAT, 1);
                        break;
                    case DARK_OAK:
                        idm.subtract(Material.DARK_OAK_BOAT, 1);
                        break;
                    default:
                        break;
                }
            default:
                break;
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onItemHang(HangingPlaceEvent event) {
        Hanging hanging = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(hanging.getWorld().getName())) {
            return;
        }

        switch (hanging.getType()) {
            case ITEM_FRAME:
                idm.subtract(Material.ITEM_FRAME, 1);
                break;
            case PAINTING:
                idm.subtract(Material.PAINTING, 1);
                break;
            default:
                break;
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBeaconUse(InventoryClickEvent event) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(event.getWhoClicked().getWorld().getName())) {
            return;
        }

        Inventory inventory = event.getClickedInventory();

        // Ignore the event if it wasn't a beacon that was clicked
        if (!(inventory instanceof BeaconInventory)) {
            return;
        }

        // If they're taking an item out of the beacon then add it back to circulation
        ItemStack oldItem = inventory.getItem(event.getSlot());
        if (oldItem != null) {
            idm.add(oldItem);
        }

        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(SynergySurvival.getPlugin(), new Runnable() {
            @Override
            public void run() {
                // If the player is putting an item in the beacon then subtract it from circulation
                // If they end up not using the item, then it'll be added back to circulation by other methods
                ItemStack item = inventory.getItem(event.getSlot());
                if (item != null) {
                    idm.subtract(item);
                }
            }
        }, 0L);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onItemEnchant(EnchantItemEvent event) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(event.getEnchantBlock().getWorld().getName())) {
            return;
        }

        // Subtract the lapis used from circulation
        idm.subtract(Material.LAPIS_LAZULI, event.whichButton() + 1);

        // If the item enchanted was a book, add an enchanted book to the circulation and subtract a regular book
        if (event.getItem().getType().equals(Material.BOOK)) {
            idm.subtract(Material.BOOK, 1);
            idm.add(Material.ENCHANTED_BOOK, 1);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerInteract(PlayerInteractEvent event) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(event.getPlayer().getWorld().getName())) {
            return;
        }

        ItemStack itemStack = event.getItem();

        // Ignore the event if there's no item
        if (itemStack == null) {
            return;
        }

        int oldAmount = itemStack.getAmount();

        switch (itemStack.getType()) {
            // If the player used one of the specified items then subtract it from circulation
            case ENDER_EYE:
            case INK_SAC:
            case FIRE_CHARGE:
            case ARMOR_STAND:
                Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(SynergySurvival.getPlugin(), new Runnable() {
                    @Override
                    public void run() {
                        // If they used >0 items then subtract the difference from circulation
                        int newAmount;
                        if (event.getHand().equals(EquipmentSlot.HAND)) {
                            newAmount = event.getPlayer().getInventory().getItemInMainHand().getAmount();
                        } else if (event.getHand().equals(EquipmentSlot.OFF_HAND)) {
                            newAmount = event.getPlayer().getInventory().getItemInOffHand().getAmount();
                        } else {
                            // Exit the method because something had to go wrong to get here
                            return;
                        }

                        if (newAmount < oldAmount) {
                            idm.subtract(itemStack, oldAmount - newAmount);
                        }
                    }
                }, 0L);
            default:
                break;
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerInteractWithEntity(PlayerInteractEntityEvent event) {
        Entity entity = event.getRightClicked();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(entity.getWorld().getName())) {
            return;
        }

        ItemStack itemStack = null;
        if (event.getHand().equals(EquipmentSlot.HAND)) {
            itemStack = event.getPlayer().getInventory().getItemInMainHand();
        } else if (event.getHand().equals(EquipmentSlot.OFF_HAND)) {
            itemStack = event.getPlayer().getInventory().getItemInOffHand();
        } else {
            // Exit the method because something had to go wrong to get here
            return;
        }

        switch (itemStack.getType()) {
            // etc
            default:
                break;
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onItemDamage(EntityDamageEvent event) {
        Entity entity = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(entity.getWorld().getName())) {
            return;
        }

        // Ignore the event if it wasn't an item that got damaged
        if (!(entity instanceof Item)) {
            return;
        }

        // Kill the item immediately so it doesn't cause more events and subtract it from circulation
        Item item = (Item) entity;
        item.remove();
        idm.subtract(item.getItemStack());
    }

    // Todo: Handle naturally generated loot chests

}
