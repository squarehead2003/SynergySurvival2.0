package net.synergyserver.synergysurvival.listeners;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.events.PacketListener;
import com.comphenix.protocol.reflect.StructureModifier;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.google.gson.JsonParser;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergysurvival.SynergySurvival;

/**
 * Listens to packets.
 */
public class PacketListeners {

    // Remove the ghost ender dragon bar
    public static final PacketListener bossBar = new PacketAdapter(SynergyCore.getPlugin(), ListenerPriority.NORMAL, PacketType.Play.Server.BOSS) {
        @Override
        public void onPacketSending(PacketEvent event) {
            // If the dragon is enabled then don't cancel the boss bar
            if (PluginConfig.getConfig(SynergySurvival.getPlugin()).getBoolean("enable_dragon")) {
                return;
            }

            StructureModifier<WrappedChatComponent> chatComponents = event.getPacket().getChatComponents();

            if (chatComponents.size() > 0) {
                String key = new JsonParser().parse(chatComponents.readSafely(0).getJson()).getAsJsonObject().get("translate").getAsString();

                if (key.equals("entity.minecraft.ender_dragon")) {
                    event.setCancelled(true);
                }
            }
        }
    };

}
