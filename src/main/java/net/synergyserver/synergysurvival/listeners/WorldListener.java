package net.synergyserver.synergysurvival.listeners;

import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergysurvival.SynergySurvival;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.event.world.WorldInitEvent;

/**
 * Listens to events that occur in the world.
 */
public class WorldListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onEndGenerate(WorldInitEvent event) {
        World world = event.getWorld();

        // Ignore the event if it doesn't take place in this plugin's world group or doesn't take place in the end.
        if (!SynergySurvival.getWorldGroup().contains(event.getWorld().getName()) || !(world.getEnvironment().equals(World.Environment.THE_END))) {
            return;
        }

        // Check the config value for if the ender dragon battle should be allowed or not
        if (!PluginConfig.getConfig(SynergySurvival.getPlugin()).getBoolean("enable_dragon")) {
            // Super ghetto way of killing the first dragon; let minecraft handle everything
            Bukkit.getScheduler().runTaskLater(SynergySurvival.getPlugin(), () -> {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "kill @e[type=ender_dragon]");
            }, 0L);
        }
    }

//    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
//    public void onEndChunkGenerate(ChunkPopulateEvent event) {
//        // Ignore the event if it doesn't take place in this plugin's world group or doesn't take place in the end.
//        if (!SynergySurvival.getWorldGroup().contains(event.getWorld().getName()) || !(event.getWorld().getEnvironment().equals(World.Environment.THE_END))) {
//            return;
//        }
//
//        // Loop over the tile entities in the chunk and check if the tile entity is a chest
//        for (BlockState blockState : event.getChunk().getTileEntities())  {
//            if (!(blockState.getBlock().getType().equals(Material.CHEST))) {
//                return;
//            }
//
//            Inventory inventory = ((Chest) blockState).getInventory();
//
//            // Loop over the items in the chest and remove items 1/2 of the time.
//            for (int i = 0; i < inventory.getSize(); i++) {
//                // If the slot is already empty then ignore it
//                if (inventory.getItem(i) == null || inventory.getItem(i).getType().equals(Material.AIR)) {
//                    continue;
//                }
//
//                if (MathUtil.randChance(0.5)) {
//                    inventory.setItem(i, new ItemStack(Material.AIR));
//                }
//            }
//        }
//    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onWeatherChange(WeatherChangeEvent event) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(event.getWorld().getName())) {
            return;
        }

        // Ignore the event if the weather is not changing to rainy
        if (!event.toWeatherState()) {
            return;
        }

        // Have a 2/3 chance to cancel the rain
        if (MathUtil.randChance(0.66)) {
            event.setCancelled(true);
        }
    }
}
