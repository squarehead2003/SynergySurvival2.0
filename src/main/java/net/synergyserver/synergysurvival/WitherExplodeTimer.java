package net.synergyserver.synergysurvival;

import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergysurvival.settings.SurvivalTieredMultiOptionSetting;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class WitherExplodeTimer extends BukkitRunnable {

    Entity witherEntity;

    public WitherExplodeTimer(Entity wither){
        this.witherEntity = wither;
    }

    @Override
    public void run() {
        if (!this.witherEntity.isValid()) {
            this.cancel();
        } else {
            for (Entity e : this.witherEntity.getNearbyEntities(3,3,3)) {
                if (e instanceof Player) {
                    // Check that their difficulty is at least extremely_easy before continuing
                    if (!SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(PlayerUtil.getProfile((Player) e), "extremely_easy")) {
                        continue;
                    }

                    this.witherEntity.getWorld().createExplosion(this.witherEntity.getLocation(), 3, true);
                    break;
                }
            }
        }
    }
}

